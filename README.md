# Guía de desarrollo de software

## Finalidad

Esta guía es una selección de buenas prácticas y consejos para el desarrollo software de calidad, no es un tutorial ni es un manual paso a paso. Si requiere guías de como programar o aprender acerca de una tecnología en especifico visite el siguiente [link.](https://www.google.cl/search?safe=off&q=como+buscar+en+google&oq=como+buscar+en+google)

## Recomendaciones

### General

- [Software](docs/recommendation/general/software.md)
- [API](docs/recommendation/general/api.md)
- [Mobile](docs/recommendation/general/mobile.md)
- [Base de datos](docs/recommendation/general/db.md)
- [infraestructura](docs/recommendation/general/infraestructura.md)
- [Microservicios](docs/recommendation/general/microservices.md)
- [SaaS](docs/recommendation/general/saas.md)

### Especifico

- [Auth0](docs/recommendation/herramientas/auth0.md)
- [JWT](docs/recommendation/herramientas/jwt.md)

## Checklist para soluciones que entraran a producción

- [General](docs/checklist/productivo/checklist.md)

## Herramientas

- [Desarrollo](docs/recommendation/tools/development.md)

## Links sugeridos

- [API-Security-Checklist](https://github.com/shieldfy/API-Security-Checklist)
- [Front-End-Checklist](https://github.com/thedaviddias/Front-End-Checklist)
- [cheatsheets](https://github.com/OWASP/CheatSheetSeries/tree/master/cheatsheets)
