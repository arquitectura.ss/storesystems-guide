# Glosario

## Listado

* _Especificación de sistemas_: En áreas como la ingeniería y la manufactura, el término especificación representa un documento técnico oficial que establezca de forma clara todas las características, los materiales y los servicios necesarios para producir componentes destinados a la obtención de productos. Estos incluyen requisitos para la conservación de dichos productos, su empaquetamiento, almacenaje y marcado así como los procedimientos para determinar su obtención exitosa y medir su calidad
