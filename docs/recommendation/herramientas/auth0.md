# OAuth

## Recomendaciones

- Siempre valida `redirect_uri` en el lado del servidor para permitir sólo ciertas URLs.
- Trata siempre de intercambiar código y no tokens (no permitas `response_type=token`).
- Usa el parámetro `state` con un hash aleatorio para prevenir CSRF en el proceso de autenticación OAuth.
- Define el ámbito (`scope`) por defecto, y valida los parámetros de ámbito para cada aplicación.
