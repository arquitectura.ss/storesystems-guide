# JWT (JSON Web Token)

## Recomendaciones

- Usa claves aleatorias complejas (`JWT Secret`) para dificultar los ataques por fuerza bruta.
- No extraigas el algoritmo del contenido. Fuerza el algoritmo en el backend (`HS256` o `RS256`).
- Haz que la expiración del token (`TTL`, `RTTL`) sea tan corta como sea posible.
- No almacenes información sensible en el contenido del JWT, puede ser descodificado [fácilmente](https://jwt.io/#debugger-io)
