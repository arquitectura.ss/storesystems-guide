# Software

## General

- Analizar si existe una solución que ya satisfaga las necesidades o alguna parte ellas.
- Se debe pensar en la posible integración con otras aplicaciones ([API](https://es.wikipedia.org/wiki/Interfaz_de_programaci%C3%B3n_de_aplicaciones), [colas](https://es.wikipedia.org/wiki/Cola_(inform%C3%A1tica)), etc).
- El desarrollo de soluciones debe ir acompañado a una análisis de requerimientos técnicos que solucionen el problema presentado ([Pruebas de rendimiento del software](https://es.wikipedia.org/wiki/Pruebas_de_rendimiento_del_software), predicciones, etc).
- Manejo de errores y logs.

## Despliegue

- Flujo automático (e.g. [CI/CD](https://en.wikipedia.org/wiki/Software_engineering), [GitOps](https://www.weave.works/technologies/gitops/), etc)
- Testing de la solución

## Documentación

- Guías de instalación
- Diseño de la solución
- Flujo de datos
- Capacitación sobre la solución
