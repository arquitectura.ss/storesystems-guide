# Base de datos

La seguridad de la base de datos hace referencia a las medidas colectivas que se utilizan para proteger los datos y las bases de datos y/o un software de gestión de bases de datos contra el uso ilegítimo y las amenazas y ataques maliciosos. Se puede profundizar en los siguientes items:

## Seguridad de los datos a nivel de sistema

- Gestionar el acceso a los datos para cada usuario de acuerdo a su rol. Por ejemplo: Administre las conexiones de usuario y servidor con Active Directory, implementando sistemas de inicio de sesión único (SSO).

## Seguridad a nivel de artefactos

- Solo entregar la información que requieren los sistemas y grupos para operar. 
- No almacenar las credenciales de la su base de datos en la aplicación

## Seguridad a nivel de datos

- Encriptación de los datos sensibles en la BD a no menos de 256 bits

## Políticas de recuperación de desastres

- Respaldo de la información cifrada y que solo se pueda restaurar en el servidor de origen.
