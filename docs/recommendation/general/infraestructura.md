# Infraestructura

## Cloud

### Orquestación

- Utilize algún método de orquestación de infraestructura.

### Despliegue

- Usar despliegue automático.

### Seguridad

- Utilize un bastion para entrar a sus servidores por puertos administrativos los cuales, después de ser utilizados, deben ser eliminados.
- Habilitar solo lo que se necesita y si es necesario utilizar una funcionalidad momentáneamente se debe deshabilitar al final.
- Usar credenciales u otro método que no sea usuario y contraseña para ingresar a los sistemas.
