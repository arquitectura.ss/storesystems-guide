# Calidad de las apps dispositivos mobiles

## Diseño visual e interacción del usuario

### Diseño

La app cumple con las pautas de diseño y utiliza patrones e íconos de IU comunes:

- La app no redefine la función prevista de un ícono del sistema (como el botón Atrás).
- La app no reemplaza un ícono del sistema con un ícono completamente diferente si este activa el comportamiento estándar de la IU.
- Si la app proporciona una versión personalizada de un ícono estándar del sistema, el ícono debe parecerse al ícono estándar del sistema y desencadenar el comportamiento estándar del sistema.
- La app no redefine ni utiliza de forma inadecuada patrones de la IU de Android, de modo que los íconos o los comportamientos pudieran desorientar o confundir a los usuarios.

### Navegación

La app admite la navegación estándar del sistema con el botón Atrás y no utiliza avisos personalizados en pantalla para el "botón Atrás".

- Todos los diálogos pueden descartar con el botón Atrás.
- Al presionar el botón de inicio en cualquier momento, se navega a la pantalla principal del dispositivo

## Funcionalidad

### Permisos

- La app solicita solo la cantidad mínima absoluta de permisos necesarios para respaldar la funcionalidad central.
- La app no solicita permisos para acceder a datos confidenciales (como contratos o el registro del sistema) ni a servicios que puedan costarle dinero al usuario (como Teléfono o SMS), a menos que esté relacionado con una capacidad central de la app.

### Ubicación de la instalación

- La app funciona normalmente cuando se la instala en una tarjeta SD.

### Audio

- No se reproduce audio cuando la pantalla está apagada, a menos que sea una función central (por ejemplo, la app es un reproductor de música).
- No se reproduce audio cuando la pantalla está bloqueada, a menos que sea una función central.
- No se reproduce audio en la pantalla principal ni cuando está activa otra app, a menos que sea una función central.
- La reproducción de audio continúa cuando la app regresa a primer plano, o le indica al usuario que la reproducción está pausada.

### IU y gráficos

- La app admite tanto la orientación horizontal como la vertical (si fuera posible).
- Las orientaciones exponen ampliamente las mismas funciones y acciones, y conservan la paridad funcional. Se aceptan cambios mínimos en el contenido o las vistas.
- La app utiliza toda la pantalla en ambas orientaciones y no usa formato de pantalla ancha para compensar los cambios de orientación.
- La app resuelve correctamente las transiciones rápidas entre las orientaciones de la pantalla sin presentar problemas.

### Estado del usuario o la app

- La app no debe dejar ningún servicio en ejecución mientras se encuentre en segundo plano, a menos que se relacione con una capacidad central (Por ejemplo, no debe dejar servicios en ejecución para mantener una conexión de red y recibir notificaciones, para mantener una conexión Bluetooth ni para mantener el GPS activado).
- La app preserva y restaura correctamente el estado del usuario o la app.
- La app preserva el estado del usuario o la app cuando abandona el primer plano y evita la pérdida accidental de datos a causa de la navegación hacia atrás y otros cambios de estado. Cuando regresa al primer plano, la app debe restaurar el estado preservado y toda transacción de estado importante que haya quedado pendiente, como cambios en campos editables, el progreso de un juego, menús, videos y otras secciones de la app o el juego.
- Cuando se reanuda la app desde el conmutador de Apps recientes, la app regresa al usuario al estado exacto en que se encontraba la última vez que este la utilizó.
- Cuando se reanuda la app después de la reactivación del dispositivo (luego de haber estado bloqueado), la app regresa al usuario al estado exacto en que se encontraba la última vez que este la utilizó.
- Cuando se reinicia desde el inicio o Todas las apps, la app restaura el estado de la app al estado anterior con la mayor precisión posible.
- Cuando se presiona Atrás, la app le da la opción al usuario de guardar el estado de la app o el usuario que, de lo contrario, se perdería al navegar hacia atrás.

## Seguridad

### Datos

- Todos los datos privados se guardan en el almacenamiento interno de la app.
- Todos los datos del almacenamiento externo se verifican antes de acceder a ellos.	
- Todos los intents y emisiones siguen recomendaciones seguras.
- No se registran datos personales o confidenciales de los usuarios en el sistema o en el registro específico de la app.

### Componentes de la app

- Solo se exportan los componentes de aplicaciones que comparten datos con otras apps, o que deberían ser invocados por otras apps (Esto incluye actividades, servicios, receptores de emisiones y especialmente proveedores de contenido).
- Todos los componentes de aplicaciones que comparten contenido con otras apps definen (y garantizan) permisos apropiados.
- Todos los proveedores de contenido que comparten contenido entre tus apps usan android:protectionLevel="signature".
- Todo el tráfico de red se envía mediante SSL.
- La aplicación declara una configuración de seguridad de red.
- Todas las bibliotecas, SDK y dependencias están actualizadas.	

### WebViews

- JavaScript está inhabilitado en todos los WebView (salvo que se requiera).
- WebView solo cargar contenido incluido en la lista blanca, si es posible.
- WebViews no usan addJavaScriptInterface() con contenido que no es de confianza.

### Ejecución

- La app no carga dinámicamente código desde afuera del APK de la app.

### Criptografía

- La app utiliza algoritmos criptográficos eficientes, proporcionados por la plataforma y no implementa algoritmos personalizados.
- La app utiliza un generador de números aleatorios seguro, en particular para inicializar claves criptográficas.

## Links
 (Developer Android)[https://developer.android.google.cn/docs/quality-guidelines/core-app-quality?hl=Es]