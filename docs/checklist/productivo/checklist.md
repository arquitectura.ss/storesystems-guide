# Checklist soluciones

Elementos que se deben considerar para poder realizar una implementación.

## General

- Los desarrollos no deben tener mensajería, configuración o propiedades del ambiente en en el código. Deben utilizarse variables de entorno, diccionarios u otros métodos.
- No reinventar la rueda (e.g.`autenticación`, `generación de tokens`, `almacenamiento de contraseñas`, `centralización de logs`), se deben implementar estándares aceptados por la comunidad.
- Validar toda información que provenga de fuentes externas a la aplicación (calidad, coherencia y consistencia de los inputs).
- Al elegir un estándar se debe seguir a cabalidad.
- No instalar elementos ni artefactos que no se utilicen, cada solución debe tener lo mínimo necesario para funcionar.
- Las soluciones deben ser agnósticas a los proveedores y tecnologías privativas.

## Autenticación y autorización

- Si es posible, utilizar métodos de autenticación diferentes usuario y contraseña.
- Validar que todos los endpoints estén protegidos con autenticación.
- Utilize políticas de límite de reintentos y funcionalidades de jailing.
- Eliminar todas las credenciales por defecto y no utilizar cuentas `root` ni supersusuario para realizar ninguna acción.

## Disponibilidad

- Actualizaciones, respaldos y modificaciones deben ser trasparentes para el usuario final.
- Solo habilitar los protocolos, métodos y accesos estrictamente necesarios (Eliminación de protocolos inseguros como FTP, TELNET y RLOGIN).
- Se debe velar por la disponibilidad de la solución para el usuario en el momento que lo requiera y garantizar métodos para recuperación de desastres.

## Integridad

- No debe existir la posibilidad de modificación de datos por personas o procesos no autorizadas.
- Se debe generar respaldos de la información relevante y esta solo deba ser accesible por la solución.
- Toda información y petición que provenga desde fuentes externas debe ser validada.
- Método de monitoreo y registro de accesos debemos poder contestar ¿Quien?, ¿Qué? y ¿Cuándo? para cada una de las acciones que se realizan.

## Confidencialidad

- Todo usuario y/o software cliente debe poder acceder solamente a los recursos que correspondan de acuerdo a su nivel de autorización.
- Toda respuesta de la aplicación debe entregar solo la información necesaria y nada más.
- Jamás se devolver información sensible cómo `credenciales`, `contraseñas`, `tokens de seguridad`.
- Nadie debe poder visualizar las contraseñas de los usuarios de la solución.

## Recomendaciones

Si tiene dudas de como poder cumplir todos estos objetivos puede revisar esta guía

- [Infraestructura](../../recommendation/contenido.md)
